*******************************************************************************
Ephemeris / PORT_LOGIN Wed Oct 18 16:49:13 2017 Pasadena, USA    / Horizons    
*******************************************************************************
Target body name: Triton (801)                    {source: nep081xl}
Center body name: Sun (10)                        {source: DE431mx}
Center-site name: BODY CENTER
*******************************************************************************
Start time      : A.D. 2015-Jan-04 01:37:00.0000 TDB
Stop  time      : A.D. 2015-Jan-04 01:47:00.0000 TDB
Step-size       : 10 minutes
*******************************************************************************
Center geodetic : 0.00000000,0.00000000,0.0000000 {E-lon(deg),Lat(deg),Alt(km)}
Center cylindric: 0.00000000,0.00000000,0.0000000 {E-lon(deg),Dxy(km),Dz(km)}
Center radii    : 696000.0 x 696000.0 x 696000.0 k{Equator, meridian, pole}    
Output units    : AU-D                                                         
Output type     : GEOMETRIC cartesian states
Output format   : 3 (position, velocity, LT, range, range-rate)
Reference frame : ICRF/J2000.0                                                 
Coordinate systm: Ecliptic and Mean Equinox of Reference Epoch                 
*******************************************************************************
JDTDB
   X     Y     Z
   VX    VY    VZ
   LT    RG    RR
*******************************************************************************
$$SOE
2457026.567361111 = A.D. 2015-Jan-04 01:37:00.0000 TDB 
 X = 2.752909819668031E+01 Y =-1.183580056197742E+01 Z =-3.911987392406793E-01
 VX=-3.252947568616605E-04 VY= 3.628764368320083E-03 VZ= 1.788027869573064E-03
 LT= 1.730816425163454E-01 RG= 2.996815741614682E+01 RR=-1.755325700282048E-03
2457026.574305556 = A.D. 2015-Jan-04 01:47:00.0000 TDB 
 X = 2.752909597845898E+01 Y =-1.183577531315080E+01 Z =-3.911863082517238E-01
 VX=-3.135388873084353E-04 VY= 3.642890914534787E-03 VZ= 1.792079696518657E-03
 LT= 1.730815722175150E-01 RG= 2.996814524428164E+01 RR=-1.750155609881374E-03
$$EOE
*******************************************************************************
Coordinate system description:

  Ecliptic and Mean Equinox of Reference Epoch

    Reference epoch: J2000.0
    XY-plane: plane of the Earth's orbit at the reference epoch
              Note: obliquity of 84381.448 arcseconds wrt ICRF equator (IAU76)
    X-axis  : out along ascending node of instantaneous plane of the Earth's
              orbit and the Earth's mean equator at the reference epoch
    Z-axis  : perpendicular to the xy-plane in the directional (+ or -) sense
              of Earth's north pole at the reference epoch.

  Symbol meaning [1 au= 149597870.700 km, 1 day= 86400.0 s]:

    JDTDB    Julian Day Number, Barycentric Dynamical Time
      X      X-component of position vector (au)                               
      Y      Y-component of position vector (au)                               
      Z      Z-component of position vector (au)                               
      VX     X-component of velocity vector (au/day)                           
      VY     Y-component of velocity vector (au/day)                           
      VZ     Z-component of velocity vector (au/day)                           
      LT     One-way down-leg Newtonian light-time (day)                       
      RG     Range; distance from coordinate center (au)                       
      RR     Range-rate; radial velocity wrt coord. center (au/day)            

Geometric states/elements have no aberrations applied.

 Computations by ...
     Solar System Dynamics Group, Horizons On-Line Ephemeris System
     4800 Oak Grove Drive, Jet Propulsion Laboratory
     Pasadena, CA  91109   USA
     Information: http://ssd.jpl.nasa.gov/
     Connect    : telnet://ssd.jpl.nasa.gov:6775  (via browser)
                  http://ssd.jpl.nasa.gov/?horizons
                  telnet ssd.jpl.nasa.gov 6775    (via command-line)
     Author     : Jon.D.Giorgini@jpl.nasa.gov
*******************************************************************************
